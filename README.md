# README #

Sample Developed iOS App(Uses CocoaPod) Portraying handling of API Calls, data management, data Modeling(JSON), Asynchronous UI Display(remote images and data display), using Rating Views .

### What is this repository for? ###

Coding Task involved the following requirements:

1.  Add network calls which are necessary to
•  Fetch the response for a search, use URL provided below
•  Download images.

https://api.flickr.com/services/rest?
method=flickr.photos.search&nojsoncallback=1&format=json&extras=url_sq,url_o&api_key=46b06b2d1cf8759a4c957ee9a87011af&per_page=25&tags=Halloween

2. Process the response
•  Parse the JSON.
•  Create model objects out of the parsed JSON. Note: You do not need to model every
property, only model those properties which you will use later.

3. Fill UI with search data
•  Download and set images on every TableViewCell, use the "url_sq" property in the
JSON response.
•  Set the corresponding title on every TableViewCell, use the "title" property in the JSON
response.

4. Configure NavigationBar.
•  Add a "Refresh" button on the right (should refresh the feed) and fix the failing UITest in the UITest Target.
•  Add a "New Search" button on the left which shows a SearchBar in the NavigationBar.
Entering text and hitting the search button should trigger a new search with the new "tag".
•  Set the view title to the current search tag.
5. Add a sort button
•  Add a button beneath the tableView, which sorts the list alphabetically (use the title property). The button should have a height of 44 and padding of 10 in all directions. Use AutoLayout if possible.
6. Data persistence
•  For time constraints, NSKeyedArchiver is used vs Coredata

### How do I get set up? ###

* Launching the ".xcworkspace" project file from Xcode.