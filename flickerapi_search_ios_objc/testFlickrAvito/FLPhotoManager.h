//
//  FLPhotoManager.h
//  FlickerTestApp
//
//  Created by Marghabi Souhail on 17/06/2015.
//  Copyright (c) 2015 Marghabi Souhail. All rights reserved.
//


#define kPhotosList @"kPhotosListkPhotosList"


#import <Foundation/Foundation.h>
#import "FLFlickerModel.h"

@class FLError, FLFlickerModel;

@interface FLPhotoManager : NSObject

+ (FLPhotoManager *)sharedInstance;

-(NSArray*)flickerPhotos;
-(void)saveProductsArray;

- (void)retrievePhotoListWithTag:(NSString*)flickrTag
                        success:(void (^)(NSArray* products))successBlock
                         failure:(void (^)(FLError *error))failureBlock;

@end
