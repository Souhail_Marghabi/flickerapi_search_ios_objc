//
//  FLError.m
//  FlickerTestApp
//
//  Created by Marghabi Souhail on 17/06/2015.
//  Copyright (c) 2015 Marghabi Souhail. All rights reserved.
//

#import "FLError.h"

@implementation FLError

+ (JSONKeyMapper *)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{ @"stat": @"status",
                                                        @"message": @"message"}];
    
}
@end
