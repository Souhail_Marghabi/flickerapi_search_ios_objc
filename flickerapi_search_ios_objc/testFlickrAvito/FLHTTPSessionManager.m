//
//  FLHTTPSessionManager.m
//  FlickerTestApp
//
//  Created by Marghabi Souhail on 17/06/2015.
//  Copyright (c) 2015 Marghabi Souhail. All rights reserved.
//

#import "AFHTTPSessionManager.h"
#import "AFURLRequestSerialization.h"
#import <AFHTTPRequestOperationManager.h>
#import "AFURLResponseSerialization.h"
#import "FLHTTPSessionManager.h"


@implementation FLHTTPSessionManager

static NSString * flickerBaseUrl = @"https://api.flickr.com";


+ (FLHTTPSessionManager*)sharedServiceInstance {
    static FLHTTPSessionManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
     sharedManager = [[self alloc] initWithBaseURL:[NSURL URLWithString:flickerBaseUrl]];
        sharedManager.requestSerializer = [[AFJSONRequestSerializer alloc] init];
        sharedManager.responseSerializer.acceptableContentTypes = [NSSet setWithArray:@[@"text/plain",@"application/json"]];
    });
    return sharedManager;
}



-(void)customPOST:(NSString *)URLString parameters:(id)parameters success:(void (^)(id))success failure:(void (^)(FLError *error))failureBlock {
    
    [super POST:URLString parameters:parameters success:^(NSURLSessionDataTask *task, NSDictionary* responseObject) {

        if (success) {
            success(responseObject);
        }
        
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [self failWithTechnicalError:error failure:failureBlock];
    } ];
    
}


-(void)customGET:(NSString *)URLString parameters:(id)parameters success:(void (^)(id))success failure:(void (^)(FLError *error))failureBlock {
    
    [super GET:URLString parameters:parameters success:^(NSURLSessionDataTask *task, NSDictionary* responseObject) {

        if (success) {
            success(responseObject);
        }
        
    }
       failure:^(NSURLSessionDataTask *task, NSError *error) {
           [self failWithTechnicalError:error failure:failureBlock];
       } ];
}
#pragma mark - Catching Error Messages and Interpreting them accordinly
- (FLError*)getErrorTemp:(NSDictionary*)responseDict{
    
    
    NSString* code = [responseDict objectForKey:@"stat"];
    
    FLError* error = [[FLError alloc] init];
    error.code = code;
    
    return error;
}

- (void)showErrorMessage:(FLError*)error {
    
    if (error.message) {
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"" message:[NSString stringWithFormat:@"%@", error.message] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alertView show];
    }
    
}


- (void)failWithTechnicalError:(NSError*)technicalError failure:(void (^)(FLError *error))failureBlock  {
    
    FLError* error = [[FLError alloc] init];
    error.code = [NSString stringWithFormat:@"%li", (long)technicalError.code];
    
    [self failWithError:error failure:failureBlock];
}

- (void)failWithError:(FLError*)error failure:(void (^)(FLError *error))failureBlock  {
    
    if (failureBlock) {
        failureBlock(error);
    }
    
}
@end
