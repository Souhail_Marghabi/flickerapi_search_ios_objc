//
//  FLFlickerModel.h
//  iOSObjcJobApplication
//
//  Created by MARGHABI on 21/01/16.
//  Copyright © 2016. All rights reserved.
//

#import "JSONModel.h"

@interface FLFlickerModel : JSONModel

@property(nonatomic, strong) NSString * photoTitle;
@property(nonatomic, strong) NSString * photoStringUrl;

-(NSURL*)photoUrl;

@end
