//
//  FLTableViewCell.h
//  testFlickrAvito
//
//  Created by Test on 04/04/16.
//  Copyright © 2016 Test. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HCSStarRatingView.h"
#import "RateView.h"
@interface FLTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *imageDescriptionLabel;
@property (weak, nonatomic) IBOutlet UIImageView *flickerCellImageView;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *productRatingView;

- (void)initRateView;
@end
