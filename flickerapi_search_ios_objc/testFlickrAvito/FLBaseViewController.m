//
//  NMBaseViewController.m
//  FlickerTestApp
//
//  Created by Marghabi Souhail on 17/06/2015.
//  Copyright (c) 2015 Marghabi Souhail. All rights reserved.
//

#import "FLBaseViewController.h"
#import <MBProgressHUD/MBProgressHUD.h>

@interface FLBaseViewController ()
@property (strong, nonatomic) MBProgressHUD *activityIndicator;
@end

@implementation FLBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self doNavBarManagement];
    
}

- (void)setTitle:(NSString *)title {
    [super setTitle:[title uppercaseString]];
}

#pragma mark -
#pragma mark ActivityIndicator

- (void)showActivityIndicator {
    [self showActivityIndicatorInView:self.view andVerb:@"Loading"];
}

- (void)showActivityIndicatorWithVerb:(NSString*)verb {
    [self showActivityIndicatorInView:self.view andVerb:verb];
}

- (void)showActivityIndicatorInView:(UIView*)view andVerb:(NSString*)verb {
    self.activityIndicator = [MBProgressHUD showHUDAddedTo:view animated:YES];
    self.activityIndicator.mode = MBProgressHUDModeIndeterminate;
    self.activityIndicator.dimBackground = NO;
    self.activityIndicator.labelText = [NSString stringWithFormat:@"%@", verb];
}

- (void)hideActivityIndicator {
    if (self.activityIndicator) {
        [self.activityIndicator hide:YES];
    }
}
#pragma mark Error Messages Handle
- (void)showErrorMessage:(FLError*)error {
    
    if (error.message) {
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"" message:[NSString stringWithFormat:@"%@", error.message] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alertView show];
    }
    
}

- (void)showErrorMessageWithMessage:(NSString*)message {
    
    if (message) {
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alertView show];
    }
    
}

#pragma mark - NavBar helper

- (void)doNavBarManagement {
    self.navigationItem.hidesBackButton = NO;
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    self.navigationController.navigationBar.topItem.title = @"";
}


@end
