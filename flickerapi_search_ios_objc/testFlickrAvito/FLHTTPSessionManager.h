//
//  FLHTTPSessionManager.h
//  FlickerTestApp
//
//  Created by Marghabi Souhail on 17/06/2015.
//  Copyright (c) 2015 Marghabi Souhail. All rights reserved.
//

#import "AFHTTPSessionManager.h"
#import "FLError.h"


@class FLError;
@interface FLHTTPSessionManager : AFHTTPSessionManager

+ (FLHTTPSessionManager*)sharedServiceInstance;

-(void)customPOST:(NSString *)URLString parameters:(id)parameters success:(void (^)(id))success failure:(void (^)(FLError *error))failureBlock ;
-(void)customGET:(NSString *)URLString parameters:(id)parameters success:(void (^)(id))success failure:(void (^)(FLError *error))failureBlock ;
@end
