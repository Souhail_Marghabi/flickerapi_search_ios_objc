//
//  FLPhotoManager.m
//  FlickerTestApp
//
//  Created by Marghabi Souhail on 17/06/2015.
//  Copyright (c) 2015 Marghabi Souhail. All rights reserved.
//

#import "FLPhotoManager.h"
#import "FLHTTPSessionManager.h"
#import "FLError.h"
#import "FLFlickerModel.h"



@interface FLPhotoManager ()

@property (nonatomic, strong) NSArray* flickerPhotos;

@property (nonatomic, strong) NSArray* fetchedProducts;

@end


@implementation FLPhotoManager




+ (FLPhotoManager*)sharedInstance {
    static FLPhotoManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[self alloc] init];
    });
    return sharedManager;
}

#pragma mark Webservice Calls

- (void)retrievePhotoListWithTag:(NSString*)flickrTag
                        success:(void (^)(NSArray* fetchedphotos))successBlock
                        failure:(void (^)(FLError *error))failureBlock {
    

    NSDictionary * parameters = @{@"method":@"flickr.photos.search",
                              @"nojsoncallback":@"1",
                              @"format":@"json",
                              @"extras":@"url_sq,url_o",
                              @"api_key":@"46b06b2d1cf8759a4c957ee9a87011af",
                              @"per_page":@"25",
                              @"tags":flickrTag};

    
    [[FLHTTPSessionManager sharedServiceInstance] customGET:@"/services/rest" parameters:parameters success:^(id responseObject){
    
        NSArray * contentArray = (NSArray*)[[responseObject valueForKey:@"photos"] valueForKey:@"photo"];

          self.flickerPhotos = contentArray;
        
          [self saveProductsArray];
        if (successBlock) {

         
            successBlock(contentArray);
        }
        
    } failure:failureBlock];
    
}

#pragma mark Local Data Handle in offline Mode 


-(NSArray*)flickerPhotos {
    NSMutableArray *productsArray = [[NSMutableArray alloc]init];
    NSMutableArray *archivedArray = [[NSUserDefaults standardUserDefaults]  objectForKey:kPhotosList];
    if (archivedArray.count !=0) {
        
        for (NSData* itemObj in archivedArray) {
            FLFlickerModel *item = [NSKeyedUnarchiver unarchiveObjectWithData: itemObj];
            [productsArray addObject:item];
        }
      self.flickerPhotos = [productsArray copy];
 
    }
   
    return _flickerPhotos;
}

-(void)saveProductsArray{
    self.fetchedProducts = [[NSArray alloc] init];
    NSUserDefaults* preferences = [NSUserDefaults standardUserDefaults];
    
    NSString* savedItems = kPhotosList;
    
    NSMutableArray *archiveArray = [NSMutableArray arrayWithCapacity:[self.flickerPhotos count]];
    
    for (FLFlickerModel* item in self.flickerPhotos) {
        NSData* itemObj = [NSKeyedArchiver archivedDataWithRootObject:item];
        [archiveArray addObject:itemObj];
        
    }
    [preferences setObject:archiveArray forKey: savedItems];
    [preferences synchronize];
}

@end
