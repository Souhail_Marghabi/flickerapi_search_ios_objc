//
//  MasterViewController.h
//  testFlickrAvito
//
//  Created by Test on 04/04/16.
//  Copyright © 2016 Test. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FLBaseViewController.h"
@class DetailViewController;

@interface FLPhotoListViewController :  FLBaseViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UISearchBarDelegate>

@property (strong, nonatomic) DetailViewController *detailViewController;

@property (nonatomic, retain)  UIBarButtonItem *refreshBarButton;

@property (strong, nonatomic) IBOutlet UITableView *flickerTableview;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *searchButton;


- (IBAction)onSortDataClick:(id)sender;

@end

