//
//  Defines.h
//  monChauffeurApp
//
//  Created by user1 on 10/05/2015.
//  Copyright (c) 2015 MonChauffeur. All rights reserved.
//



#ifndef monChauffeurApp_Defines_h
#define monChauffeurApp_Defines_h
#define ZPTEST_LOCALIZED_FILE @"Localizable"
#define kLoginEmailDefaultKey @"kLoginEmailDefaultKey"
#define kNotificationToken @"kNotificationToken"
#define kCommandesListKey @"kCommandesListKey"
#define kSortedListKey @"kSortedListKey"
#define kCommandesDetailKey @"kCommandesDetailKey"
#define kUserDefaultKey @"kUserDefaultKey"
#define kUserCredits @"kUserCredits"
#define kUserID @"kUserID"
#define kNotificationData @"kNotificationData"

#define APP_DELEGATE ((AppDelegate *) [[UIApplication sharedApplication] delegate])



#endif
