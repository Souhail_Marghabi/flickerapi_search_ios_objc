//
//  NSArray+additions.h
//  iOSObjcJobApplication
//
//  Created by Test on 21/01/16.
//  Copyright © 2016 willhaben.at. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (additions)
- (NSArray *)newArrayWithDescriptorKey:(NSString *)descriptorKey ascending:(BOOL)acending;
@end
