//
//  FLError.h
//  FlickerTestApp
//
//  Custom object to capture Error codes and messages and handling them in REST Requests FailureBlock
//  Created by Marghabi Souhail on 17/06/2015.
//  Copyright (c) 2015 Marghabi Souhail. All rights reserved.
//
#import "JSONModel.h"

@interface FLError : JSONModel
@property (nonatomic, strong) NSString* code;
@property (nonatomic, strong) NSString* status;
@property (nonatomic, strong) NSString* message;

@end
