//
//  MasterViewController.m
//  testFlickrAvito
//
//  Created by Test on 04/04/16.
//  Copyright © 2016 Test. All rights reserved.
//

#import "FLPhotoManager.h"
#import "FLPhotoListViewController.h"
#import "AFNetworkReachabilityManager.h"
#import "FLTableViewCell.h"
#import "FLPhotoManager.h"
#import "FLFlickerModel.h"
#import "NSArray+additions.h"
#import  <AFNetworking/UIImageView+AFNetworking.h>


@interface FLPhotoListViewController ()

@property (nonatomic, strong) UISearchBar *searchView;
@property NSMutableArray *objects;
@property NSMutableArray *fetchedItems;
@property (strong, nonatomic) NSArray *filteredList;
@property NSMutableArray *tableDataSource;
@property (strong, nonatomic) NSArray *photoList;

@property (strong, nonatomic) UIImageView *titleHeaderImageView;
@end

@implementation FLPhotoListViewController

typedef NS_ENUM(NSInteger, ZPDataSearchScope)
{
    searchScopeProductType = 0,
    searchScopeProductNature = 1
};

#pragma mark - WS Load Photos
- (void)loadPhotosWithTag:(NSString*)tag {
    _fetchedItems = [NSMutableArray new];
    _tableDataSource = [NSMutableArray new];
    self.photoList = nil;
    [self.fetchedItems removeAllObjects];
    if (!tag || [tag isEqualToString:@""]) {
        tag = @"Halloween";
        
    }
    
    self.title = [NSString stringWithFormat:@"#%@", tag];
    [[FLPhotoManager sharedInstance] retrievePhotoListWithTag:tag success:^(NSArray *products) {
        
        [super hideActivityIndicator];
        
        [[FLPhotoManager sharedInstance] saveProductsArray];
        self.photoList =products;
        
        [self.fetchedItems addObjectsFromArray:products];
        //parcour the array of photos to create objects
        for (NSDictionary * item in self.fetchedItems) {
            FLFlickerModel * flickerPhoto = [[FLFlickerModel alloc] initWithDictionary:item error:nil];
            //add object to our data array
            [_tableDataSource addObject:flickerPhoto];
        }
        [self.flickerTableview reloadData];
        
    } failure:^(FLError *error) {
        [super hideActivityIndicator];
        if (![AFNetworkReachabilityManager sharedManager].reachable) {
            
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Please Verify Connectivity" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            self.photoList =[[FLPhotoManager sharedInstance] flickerPhotos];
            [self.fetchedItems addObjectsFromArray:self.photoList];
            [self.flickerTableview reloadData];
            [alertView show];
        }else
        {
            [self showErrorMessage:error];
        }
        
    }];
    
}


#pragma mark - ViewLifeCycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupSearchBar];
    
    self.refreshBarButton = [[UIBarButtonItem alloc] initWithTitle:@"Refresh" style:UIBarButtonItemStylePlain target:self action:@selector(onRefreshButtonClick:)];
    self.navigationItem.rightBarButtonItem = _refreshBarButton;
    // Do any additional setup after loading the view, typically from a nib.
    
    [self.flickerTableview registerNib:[UINib nibWithNibName:@"FLTableViewCell" bundle:nil]forCellReuseIdentifier:@"photoTableCell"];
    
    [self.flickerTableview reloadData];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [super showActivityIndicator];
    [self loadPhotosWithTag:@""];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - User Touch Event
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
    
}

#pragma mark - Segues for Storyboard

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
}

#pragma mark - Table View Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.tableDataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    FLTableViewCell * flCell =  [tableView dequeueReusableCellWithIdentifier:@"photoTableCell" forIndexPath:indexPath];
    NSDate *object = self.objects[indexPath.row];
    
    FLFlickerModel * flickerPhoto = _tableDataSource[indexPath.row];
    [flCell.flickerCellImageView setImageWithURL:[flickerPhoto photoUrl] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    [flCell initRateView];
    
    // Testing Rating View  Values
    
    if(indexPath.row %2 == 0){
        flCell.productRatingView.value = 2.5f;
    }
    else{
        flCell.productRatingView.value = 4.0f;
    }

    
    flCell.imageDescriptionLabel.text=[object description];
    flCell.imageDescriptionLabel.text = flickerPhoto.photoTitle;
    return flCell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self.objects removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }
}

#pragma mark Refresh & Sort Button Event Handle

- (IBAction)onRefreshButtonClick:(id)sender {
    [super showActivityIndicatorWithVerb:@"Refreshing Data"];
    
    [self loadPhotosWithTag:@""];
    
}

- (IBAction)onSortDataClick:(id)sender {
    
    _tableDataSource = [[NSMutableArray alloc] initWithArray:
                        [_tableDataSource newArrayWithDescriptorKey:@"photoTitle"
                                                          ascending:YES]];
    
    [self.flickerTableview reloadData];
}

#pragma mark SearchBar Delegate Methods

-(void)setupSearchBar
{
    self.searchView = [[UISearchBar alloc] init];
    _searchView.showsCancelButton = YES;
    _searchView.delegate = self;
}
- (IBAction)searchSpecificTaggedContent:(id)sender {
    
    [UIView animateWithDuration:0.5 animations:^{
        
    } completion:^(BOOL finished) {
        
        self.navigationItem.rightBarButtonItem =nil;
        self.navigationItem.titleView = _searchView;
        
        [UIView animateWithDuration:0.5
                         animations:^{
                             _searchView.alpha = 1;
                             
                         } completion:^(BOOL finished) {
                             
                             [_searchView becomeFirstResponder];
                         }];
        
    }];
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    
    [searchBar resignFirstResponder];
    
    UIBarButtonItem *refreshSearchButton = [[UIBarButtonItem alloc] initWithTitle:@"Refresh"
                                                                            style:UIBarButtonItemStylePlain target:self action:@selector(onRefreshButtonClick:)];
    
    
    [UIView animateWithDuration:0.5f animations:^{
        _searchView.alpha = 0.0;
    } completion:^(BOOL finished) {
        self.navigationItem.titleView = nil;
        //self.navigationItem.rightBarButtonItem = _refreshBarButton;
        self.navigationItem.rightBarButtonItem = refreshSearchButton;
        
    }];
    
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    self.navigationController.title = searchBar.text;
    [super showActivityIndicator];
    [self loadPhotosWithTag:searchBar.text];
}


@end
