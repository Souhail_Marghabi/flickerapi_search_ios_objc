//
//  FLTableViewCell.m
//  testFlickrAvito
//
//  Created by Test on 04/04/16.
//  Copyright © 2016 Test. All rights reserved.
//

#import "FLTableViewCell.h"

@implementation FLTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)initRateView {

    self.productRatingView.emptyStarImage = [UIImage imageNamed:@"gray.png"];
    self.productRatingView.filledStarImage =[UIImage imageNamed:@"red.png"];
    self.productRatingView.allowsHalfStars = YES;
    self.productRatingView.accurateHalfStars = YES;
    self.productRatingView.maximumValue = 4;
    self.productRatingView.minimumValue = 0;
    self.productRatingView.value = 0;
    self.productRatingView.userInteractionEnabled= false;
}


@end
