//
//  FLFlickerModel.m
//  iOSObjcJobApplication
//
//  Created by MARGHABI on 21/01/16.
//  Copyright © 2016. All rights reserved.
//

#import "FLFlickerModel.h"

@implementation FLFlickerModel
+ (JSONKeyMapper *)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{ @"title": @"photoTitle",
                                                        @"url_sq": @"photoStringUrl"}];
    
}

+ (BOOL)propertyIsOptional:(NSString *)propertyName
{
    return YES;
}

-(NSURL*)photoUrl
{
    return [NSURL URLWithString:_photoStringUrl];
}

@end
