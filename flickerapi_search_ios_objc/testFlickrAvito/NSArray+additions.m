//
//  NSArray+additions.m
//  iOSObjcJobApplication
//
//  Created by Test on 21/01/16.
//  Copyright © 2016 willhaben.at. All rights reserved.
//

#import "NSArray+additions.h"

@implementation NSArray (additions)

- (NSArray *)newArrayWithDescriptorKey:(NSString *)descriptorKey ascending:(BOOL)acending
{
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:descriptorKey ascending:acending];
    
    return [self sortedArrayUsingDescriptors:@[sortDescriptor]];
}


@end
