//
//  NMBaseViewController.h
//  FlickerTestApp
//
//  Created by Marghabi Souhail on 17/06/2015.
//  Copyright (c) 2015 Marghabi Souhail. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FLError.h"

@interface FLBaseViewController : UIViewController


// Loader
- (void)showActivityIndicator;
- (void)showActivityIndicatorWithVerb:(NSString*)verb;

- (void)hideActivityIndicator;
- (void)showErrorMessage:(FLError*)error;
- (void)showErrorMessageWithMessage:(NSString*)message;


@end
